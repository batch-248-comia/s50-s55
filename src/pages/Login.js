import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login(){


	//define state hooks for all input fields

	
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	
	const [isActive, setIsActive] = useState(false);

	
	console.log(email);
	console.log(password1);
	

	//Function to simulate user registration

	function authenticate(e){

		//prevents page redirection via form submission
		//e.preventDefault();
		//Comment out for us to allow the page to render automatically after the submit button was triggered

		//set the email of the authenticated user in the local storage
		/*
			Syntax: 
			localStorage.setItem("propertyName",value)
		*/

		localStorage.setItem("email", email);

		//clear input fields
		
		setEmail("");
		setPassword1("");
		
		alert(`Welcome back, ${email}!`)


	}

	useEffect(()=>{

		//validation to enable the submit button
		if(email !== "" &&  password1 !== "" ){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[email, password1])

	return(
		/*
			Bind the user input states into their corresponding input fields via "onChange" event handler and set the value of the form input fields via the 2-way binding
		*/

	<>
		<h1>Login</h1> 

			<Form onSubmit={(e)=>authenticate(e)}>
							    
			    <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email Address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter Email" 
			        	value = {email}
			        	onChange = {e=>setEmail(e.target.value)}
			        	required/>
			        <Form.Text className="text-muted">
          				We'll never share your email with anyone else.
        			</Form.Text>
			    </Form.Group>

			    
			    <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password" 
			        	value = {password1}
			        	onChange = {e=>setPassword1(e.target.value)}
			        	required/>
			    </Form.Group>

			    			   
			   {isActive ?

			   <Button variant="primary" type="submit" id="submitBtn">
			        Submit
			   </Button>

			   :

			   <Button variant="primary" type="submit" id="submitBtn" disabled>
			        Submit
			   </Button>


			   }
			    
			    </Form>
	</>

	)

}
import courses from "../data/courses";
import CourseCard from '../components/CourseCard';


export default function Courses(){

	//check to see if the mock data was captured
	//console.log(courses);
	//console.log(courses[0]);

	const coursesMap = courses.map(course=>{
		return(

			<CourseCard key={course.id} courseProp={course}/>

		)

	})

	return(
		<>
			<h1>Courses</h1>
			{coursesMap}
		</>
		)
}
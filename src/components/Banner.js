//import Row from 'react-bootstrap/Row';
//import Col from 'react-bootstrap/Col';

import { Row, Col, Button } from "react-bootstrap";

export default function Banner(){

	return(

		<Row>
			<Col>
				<h1>B248 Booking App</h1>
				<p>Opportunities for everyone, everywhere!</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
		)
}


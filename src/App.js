import {Container} from 'react-bootstrap';

import { BrowserRouter as Router } from 'react-router-dom';
/*
    BrowserRouter is a component provided by the React Router library
    Allows us to build SPA with multiple views or pages that can be navigated without refreshing the browser
*/

import { Routes, Route } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import './App.css';

function App() {
  return (
    <Router>
     <AppNavbar/>
     <Container>
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/courses" element={<Courses/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="*" element={<NotFound/>}/>
          <Route path="/register" element={<Register/>} />
         </Routes>
     </Container>
    </Router>
  );
}

export default App;
